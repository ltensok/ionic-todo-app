import { AuthService } from './../../services/auth';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { reorderArray } from 'ionic-angular';

@Injectable()
export class DataProvider {

  todos: any = [];
  dbToken = 'KYy5C4oLRpjULvYsUAGSHfXg3UH6Bonuc4TZ9DI9';

  constructor(public http: HttpClient,
              public storage: Storage,
              private authService: AuthService) {
    console.log('Hello DataProvider Provider');
  }

  load() {
    //Loome lubaduse (promise), mis tagastab meile millalgi tulevikus andmeid
    return new Promise(resolve => {
      if (this.todos.length > 0) {
        resolve(this.todos);
      } else {
        // Meil pole veel midagi tagastada, seega ei tee midagi
        //resolve(undefined);

        this.storage.get('todoData').then((todos) => {
          if (todos && typeof(todos) !== "undefined") {
            this.todos = todos;
          }

          resolve(this.todos);
        });

      }
    });
  }

/*   save(token: string, todo) {
    // Kui leiame oma elementide hulgast samasuguse, siis muudame seda, vastasel juhul lisame
    let index = this.todos.indexOf(todo);

    if (index === -1) {
      this.todos.push(todo);
    } else {
      this.todos[index] = todo;
    }

    this.storage.set('todoData', this.todos);
  } */

  save(todo) {
    const userId = this.authService.getActiveUser().uid;
    return this.http
      .post('https://todo-app-621f6.firebaseio.com/' + userId + '/' + 'todos.json?auth=' + this.dbToken, todo)
      .subscribe(res => {
        console.log(res);
      }, xhr => {
        console.log(xhr);
      })
  }

  removeTodo(todo) {
    let index = this.todos.indexOf(todo);

    if (index !== -1) {
      this.todos.splice(index, 1);
    }

    this.storage.set('todoData', this.todos);
  }

  reorderTodos(indexes) {
    // let element = this.todos[indexes.from];
    // this.todos.splice(indexes.from, 1);
    // this.todos.splice(indexes.to, 0, element);

    this.todos = reorderArray(this.todos, indexes);

    this.storage.set('todoData', this.todos);
  }

}
